package filesync;

/**
 * @author aaron
 * @date 7th April 2013
 */

import java.io.File;
import java.io.IOException;
import java.util.concurrent.CountDownLatch;

/*
 * SyncTest is a test class. It is not a client/server program.
 * Both the sender and receiver are maintained locally.
 * Two file names should be given as arguments: fromFile
 * and toFile.
 * SyncTest will call the synchroniser every
 * 5 seconds to copy changes from fromFile to toFile.
 */

public class SyncTest {
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		SynchronisedFile fromFile=null,toFile=null;
		
		/*
		 * Initialise the SynchronisedFiles.
		 */
		try {
			fromFile=new SynchronisedFile("/Users/Egmont/Documents/MSE/Development/Workspase/COMP90015Asgt1/src/filesync/from/from.txt");
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(-1);
		}
		
		try {
			toFile=new SynchronisedFile("/Users/Egmont/Documents/MSE/Development/Workspase/COMP90015Asgt1/src/filesync/to/to.txt");
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(-1);
		}
		
		/*
		 * Start a thread to service the Instruction queue.
		 */
		Thread stt = new Thread(new SyncTestThread(fromFile,toFile));
		stt.start();
		
		/*
		 * Continue forever, checking the fromFile every 5 seconds.
		 */

		while(true){
			try {
				// TODO: skip if the file is not modified

				System.err.println("SyncTest: calling fromFile.CheckFileState()");
				fromFile.CheckFileState();
				
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(-1);
			} catch (InterruptedException e) {
				e.printStackTrace();
				System.exit(-1);
			}
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
				System.exit(-1);
			}
		}
		
		
	}

}
