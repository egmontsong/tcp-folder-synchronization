package SyncClient;


import java.io.IOException;
import filesync.*;

/*
 * This test thread provides comments to explain how the Client/Server
 * architecture could be implemented that uses the file synchronisation protocol.
 */

public class ClientInstService implements Runnable {

    SynchronisedFile syncfile; // this would be on the Client
    ClientTCPService clientTcp;
    
    ClientInstService(SynchronisedFile syncfile, ClientTCPService clientTcp){
        this.syncfile = syncfile;
        this.clientTcp= clientTcp;
    }
    

    
    public synchronized void sendInst() throws InterruptedException, IOException {
    	Instruction inst; 
    	if(!syncfile.instQ.isEmpty()){
    		clientTcp.occupyClient();
//    		System.out.println("source file:"+syncfile.getFilename());
    		while(clientTcp.isOccupied && !syncfile.instQ.isEmpty()){
        		inst = syncfile.NextInstruction();
        		clientTcp.passInst(inst);
        	}
    		clientTcp.freeClient();
//    		System.out.println("finished");
    	}    
    }
    
    @SuppressWarnings("null")
	@Override
    public void run() {
    	while(true){
    		try {
    			sendInst();
    		} catch (InterruptedException | IOException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
    	}
    }
}
