package SyncClient;
/*
 * Copyright (c) 2008, 2010, Oracle and/or its affiliates. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   - Neither the name of Oracle nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

import java.nio.file.*;

import static java.nio.file.StandardWatchEventKinds.*;
import java.nio.file.attribute.*;
import java.io.*;
import java.util.*;
import filesync.SynchronisedFile;


/**
 * Example to watch a directory (or tree) for changes to files.
 */

public class WatchDir implements Runnable {

    private final WatchService watcher;
    private final Map<WatchKey,Path> keys;
    private final boolean recursive;
    private boolean trace = false;
    
    public List<SynchronisedFile> SyncFileList = new ArrayList<SynchronisedFile>();
    public List<Thread> SyncInstServiceList = new ArrayList<Thread>();
    ClientTCPService clientTcp = new ClientTCPService();
    
    @SuppressWarnings("unchecked")
    static <T> WatchEvent<T> cast(WatchEvent<?> event) {
        return (WatchEvent<T>)event;
    }

    /**
     * Register the given directory with the WatchService
     */
    private void register(Path dir) throws IOException {
        WatchKey key = dir.register(watcher, ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY);
        if (trace) {
            Path prev = keys.get(key);
            if (prev == null) {
//                System.out.format("register: %s\n", dir);
            } else {
                if (!dir.equals(prev)) {
//                    System.out.format("update: %s -> %s\n", prev, dir);
                }
            }
        }
        keys.put(key, dir);
    }

    /**
     * Register the given directory, and all its sub-directories, with the
     * WatchService.
     */
    private void registerAll(final Path start) throws IOException {
        // register directory and sub-directories
        Files.walkFileTree(start, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs)
                throws IOException
            {
                register(dir);
                return FileVisitResult.CONTINUE;
            }
        });
    }

    /**
     * Creates a WatchService and registers the given directory
     */
    WatchDir(Path dir, boolean recursive, String host, int port) throws IOException {
        this.watcher = FileSystems.getDefault().newWatchService();
        this.keys = new HashMap<WatchKey,Path>();
        this.recursive = recursive;
		clientTcp.createTCPclient(host, port);
        
        if (recursive) {
//            System.out.format("Scanning %s ...\n", dir);
            registerAll(dir);
//            System.out.println("Done.");
        } else {
            register(dir);
        }
//        while(keys.values().iterator().hasNext()){
//        	System.out.println(keys.values().iterator().next());
//        }
        
        // enable trace after initial registration
        this.trace = true;

    }

    /**
     * Process all events for keys queued to the watcher
     * @throws IOException 
     * @throws InterruptedException 
     */
	@SuppressWarnings("deprecation")
	void processEvents() throws IOException, InterruptedException {
    	System.out.println("DirWatcher is starting to watch the folder!");
        for (;;) {

            // wait for key to be signalled
            WatchKey key;
            try {
                key = watcher.take();
            } catch (InterruptedException x) {
                return;
            }

            Path dir = keys.get(key);
            if (dir == null) {
                System.err.println("WatchKey not recognized!!");
                continue;
            }

            for (WatchEvent<?> event: key.pollEvents()) {
                WatchEvent.Kind kind = event.kind();

                // TBD - provide example of how OVERFLOW event is handled
                if (kind == OVERFLOW) {
                    continue;
                }

                // Context for directory entry event is the file name of entry
                WatchEvent<Path> ev = cast(event);
                Path name = ev.context();
                Path child = dir.resolve(name);

                // print out event
//                System.out.format("%s: %s\n", event.kind().name(), child);
//                System.out.println(Files.isDirectory(child));             
                if (Files.isDirectory(child)) {
				    System.err.println("Synchronised object must be a file!");
				}
                if (kind == ENTRY_CREATE && !Files.isDirectory(child)) {
                	if (child.toFile() != null){
                    	SyncFileList.add(new SynchronisedFile(child.toString()));
//                    	System.out.println(SyncFileList.get(SyncFileList.size()-1));
                    	SyncInstServiceList.add(new Thread(new ClientInstService(SyncFileList.get(SyncFileList.size()-1),clientTcp)));
//                    	System.out.println(SyncInstServiceList.get(SyncInstServiceList.size()-1));
                    	SyncInstServiceList.get(SyncInstServiceList.size()-1).setDaemon(true);
                    	SyncInstServiceList.get(SyncInstServiceList.size()-1).start();
                    	SyncFileList.get(SyncFileList.size()-1).threadName = SyncInstServiceList.get(SyncInstServiceList.size()-1).getName();
                    	SyncFileList.get(SyncFileList.size()-1).CheckFileState();
                	}
                }
                if (kind == ENTRY_MODIFY && !Files.isDirectory(child)) {
                	if (child.toFile() != null){
                    	for(SynchronisedFile temp : SyncFileList){
                			if (temp.getFilename().equals(child.toAbsolutePath().toString())){
                				temp.CheckFileState();
                			}
                		}
                	}
                }
                if (kind == ENTRY_DELETE && !Files.isDirectory(child)) {
	                	String removeFilename = null;
	                	String removeThread = null;
	                	for(SynchronisedFile temp : SyncFileList){
	            			if (temp.getFilename().equals(child.toAbsolutePath().toString())){		
	            					for(Thread tempt : SyncInstServiceList){
	                					if (tempt.getName().equals(temp.threadName)){
	                						tempt.stop();
	                					}
	                				}   				
	            			}
	            		}
	                	for (Iterator<SynchronisedFile> iter = SyncFileList.listIterator(); iter.hasNext(); ) {
	                	    SynchronisedFile a = iter.next();
	                	    if (a.getFilename().equals(child.toAbsolutePath().toString())) {
	                	    	removeFilename = a.getFilename().replace(dir.toString()+"/", "");
	                	    	iter.remove();               	    	
	                	    }
	                	}
	                	if(removeFilename!=null){
	                    	clientTcp.occupyClient();
	                    	clientTcp.passDeleteMsg(removeFilename);
	                		clientTcp.freeClient();
	                	}
	                	else{
	                		System.err.println("Synchronised object must be a file!");
	                	}
                }
                // if directory is created, and watching recursively, then
                // register it and its sub-directories
            }

            // reset key and remove from set if directory no longer accessible
            boolean valid = key.reset();
            if (!valid) {
                keys.remove(key);

                // all directories are inaccessible
                if (keys.isEmpty()) {
                    break;
                }
            }
        }
    }

    static void usage() {
        System.err.println("usage: java WatchDir [-r] dir");
        System.exit(-1);
    }
    
    public void createSycnfileForAll(Path dir) throws IOException, InterruptedException{
    	File folder = new File(dir.toString());
    	File[] listOfFiles = folder.listFiles();
    		System.out.println("Synchronising existing files:");
    	    for (int i = 0; i < listOfFiles.length; i++) {
    	      if (listOfFiles[i].isFile()) {
    	    	  if (!listOfFiles[i].getName().equals(".DS_Store")){
//    	    		  System.out.println("File " + listOfFiles[i].getName());
    	    		    Path filepath = Paths.get(dir.toString(),listOfFiles[i].getName());
    	    	        SyncFileList.add(new SynchronisedFile(filepath.toString()));
    	    	        SyncInstServiceList.add(new Thread(new ClientInstService(SyncFileList.get(SyncFileList.size()-1),clientTcp)));
    	    	        SyncInstServiceList.get(SyncInstServiceList.size()-1).setDaemon(true);
    	    	    	SyncInstServiceList.get(SyncInstServiceList.size()-1).start();
    	    	    	SyncFileList.get(SyncFileList.size()-1).threadName = SyncInstServiceList.get(SyncInstServiceList.size()-1).getName();
    	    	    	String createFilename = listOfFiles[i].getName();
    	    	    	clientTcp.occupyClient();
                    	clientTcp.passCreateMsg(createFilename);
                		clientTcp.freeClient();
                		SyncFileList.get(SyncFileList.size()-1).CheckFileState();    		
    	    	  }
    	      } else if (listOfFiles[i].isDirectory()) {
//    	        System.out.println("Directory " + listOfFiles[i].getName());
    	      }
    	    }

    }

	@Override
	public void run() {
		// TODO Auto-generated method stub
			try {
				processEvents();
			} catch (IOException | InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	}

//    public static void main(String[] args) throws IOException {
//        // parse arguments
////        if (args.length == 0 || args.length > 2)
////            usage();
//        boolean recursive = false;
////        int dirArg = 0;
////        if (args[0].equals("-r")) {
////            if (args.length < 2)
////                usage();
////            recursive = true;
////            dirArg++;
////        }
//        String dirpath = "/Users/Egmont/Documents/MSE/Development/Workspase/COMP90015Asgt1/src/filesync/from/";
//        // register directory and process its events
//        Path dir = Paths.get(dirpath);
//        System.out.println(dir);
//        new WatchDir(dir, recursive).processEvents();
//    }
}
