package SyncClient;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

import filesync.*;

/*
 * This test thread provides comments to explain how the Client/Server
 * architecture could be implemented that uses the file synchronisation protocol.
 */

public class ClientTCPService {

	DataInputStream in = null;
	DataOutputStream out = null;
	Socket socket = null;
    boolean isOccupied = false; 
    boolean processing = false;
	public void createTCPclient(String host, int port) throws UnknownHostException, IOException {
		// Get the streams for reading/writing to the socket
		this.socket = new Socket(host, port);
		this.in = new DataInputStream(socket.getInputStream());
		this.out = new DataOutputStream(socket.getOutputStream());
	}
	
	public synchronized void occupyClient(){
		while (isOccupied == true) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		isOccupied = true;
		notifyAll();
	}
    
	public synchronized void freeClient(){
		while (isOccupied == false) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		isOccupied = false;
		notifyAll();
	}
	
    public synchronized void passInst(Instruction inst) throws IOException {
    	Instruction instcopy = null;
    	try {
    		if(!processing){
    			  String msg = inst.ToJSON();
    			  instcopy = inst;
    			  out.writeUTF(msg);
    			  out.flush();
    		}

			// Blocks until server responds
			
			if (in.readUTF().contains("need resend")){
				processing = true;
				Instruction upgraded=new NewBlockInstruction((CopyBlockInstruction)instcopy);
				String msg2 = upgraded.ToJSON();
				out.writeUTF(msg2);
				if (in.readUTF().contains("process successfully")){
					processing = false;
//					System.out.println("process successfully");
				}
			}	
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.err.println("Connection stopped!");
		}
    }
    
    public synchronized void passDeleteMsg(String msg) throws IOException{
    	try {
//    		  System.err.println("File is deleted on client:"+ msg);
    		  out.writeUTF("File is deleted on client:"+ msg);
    		  out.flush();		  
    		  
    		  String response = in.readUTF();
//    		  System.out.println(response);
    	} catch (IOException e) {
    		System.err.println("Connection stopped!");
    	}

    }
    
    public synchronized void passCreateMsg(String msg) throws IOException{
    	try{
//  		  System.err.println("File needs to synchronise on server:"+ msg);
  		  out.writeUTF("File needs to synchronise on server:"+ msg);
  		  out.flush();		  
  		  
  		  String response = in.readUTF();
//  		  System.out.println(response);
    	} catch (IOException e) {
    		System.err.println("Connection stopped!");
    	}


  }
    
}
