package SyncServer;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import filesync.SynchronisedFile;

public class SyncDir{
    public List<SynchronisedFile> SyncFileList = new ArrayList<SynchronisedFile>();
    Path dir = null;
    
    SyncDir(Path dir){
    	this.dir = dir;
    }
	
    public void createSycnfileForAll() throws IOException, InterruptedException{
//    	System.out.println("SyncDir is starting to watch folder!");
    	File folder = new File(dir.toString());
    	File[] listOfFiles = folder.listFiles();
//    	System.out.println("Here is the existing files:");
	    for (int i = 0; i < listOfFiles.length; i++) {
	      if (listOfFiles[i].isFile()) {
	    	  if (!listOfFiles[i].getName().equals(".DS_Store")){
	    		  Path filepath = Paths.get(dir.toString(),listOfFiles[i].getName());
	    	      SyncFileList.add(new SynchronisedFile(filepath.toString()));
	    	  }        
	      } else if (listOfFiles[i].isDirectory()) {
	        System.err.println("Directory will not be synchronised!");
	      }
	    }	    
    }
}