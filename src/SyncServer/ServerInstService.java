package SyncServer;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import filesync.*;

/*
 * This test thread provides comments to explain how the Client/Server
 * architecture could be implemented that uses the file synchronisation protocol.
 */

public class ServerInstService {

    public List<SynchronisedFile> syncfilelist; // this would be on the Client
    Path dir = null;
    static int port = 0;
    DataInputStream in = null;
	DataOutputStream out = null;
	Socket socket = null;
	ServerSocket server = null;
	
    ServerInstService(List<SynchronisedFile> syncfilelist, Path dir, int port){
        this.syncfilelist=syncfilelist;
        this.dir = dir;
        this.port = port;
    }
    
    public void createTCPserver() throws IOException {
		// Open up a server socket at port 4444 on localhost
		this.server = new ServerSocket(port);
			// In a infinite loop - keep accepting clients
		System.err.println("Server starts");
		while(true){
			// Blocks until a client comes
			socket = server.accept();
			System.err.println("Connection Statu:"+socket.isConnected());
			Thread t = new Thread(() -> serveClient(socket));
			t.setDaemon(true);
			t.start();
		}
	}

	/**
	 * Serves a single client - identified by the socket.
	 * @param socket
	 */
	private void serveClient(Socket socket) {
		try (Socket clientSocket = socket) {
			// Get the in/out streams of the sockets
			 this.in = new DataInputStream(clientSocket.getInputStream());
			 this.out = new DataOutputStream(clientSocket.getOutputStream());			
			InstructionFactory instFact = new InstructionFactory();
			SynchronisedFile currentSyncfile = null;
			// In infinite loop - keeps reading and sending to client
			while(true) {
				// Blocks until client sends a message
				String original = in.readUTF();		
				if (original.contains("File is deleted on client:")){
					String filename = original.replace("File is deleted on client:", "");
					Path path = Paths.get(dir.toString(),filename);
					deleteMirroFile(path);
					for (Iterator<SynchronisedFile> iter = syncfilelist.listIterator(); iter.hasNext(); ) {
                	    SynchronisedFile a = iter.next();            	  
                	    if (a.getFilename().equals(path.toString())) {
                	    	iter.remove();
                	    }
                	}
					out.writeUTF(String.format("File is deleted on server!"));
					out.flush();
				}
				else if (original.contains("File needs to synchronise on server:")){
					String filename = original.replace("File needs to synchronise on server:", "");
					Path path = Paths.get(dir.toString(),filename);
					createMirroFile(path);
//					System.out.println(path);
					syncfilelist.add(new SynchronisedFile(path.toString()));
					currentSyncfile = syncfilelist.get(syncfilelist.size()-1);
					out.writeUTF(String.format("File is copied on server!"));
					out.flush();
				}
				else{
					Instruction inst = instFact.FromJSON(original);	
					
//					System.out.println("received"+inst.ToJSON());
//					System.out.println(inst.Type().equals("StartUpdate"));
					if(inst.Type().equals("StartUpdate")){
						currentSyncfile = null;
						JsonObject jobj = new Gson().fromJson(inst.ToJSON(), JsonObject.class);
						String filename = jobj.get("FileName").toString().replace("\"", "");
	    				Path path = Paths.get(dir.toString(),filename);
						for (SynchronisedFile temp : syncfilelist){
							if (temp.getFilename().contains(filename)){
								currentSyncfile = temp;
							}
						}	
						if (currentSyncfile == null){
							createMirroFile(path);
							syncfilelist.add(new SynchronisedFile(path.toString()));
							currentSyncfile = syncfilelist.get(syncfilelist.size()-1);
						}
						try{
							currentSyncfile.ProcessInstruction(inst);
							out.writeUTF("process successfully");
							out.flush();
						} catch (IOException e) {
							e.printStackTrace();
							System.exit(-1); // just die at the first sign of trouble
						} catch (BlockUnavailableException e) {
							out.writeUTF("process failed");
							out.flush();
//							e.printStackTrace();
						}
						
					}
					else{
						try{
							currentSyncfile.ProcessInstruction(inst);
							out.writeUTF("process successfully");
							out.flush();
						} catch (IOException e) {
							e.printStackTrace();
							System.exit(-1); // just die at the first sign of trouble
						} catch (BlockUnavailableException e) {
							out.writeUTF(String.format("need resend"));
							out.flush();
						}						
					}
				}
			}
		} catch (IOException e1) {
			System.err.println("Connection Stopped!");
		} 
	}
	
	private void createMirroFile(Path path) throws IOException{

//    	System.out.println(path);
    	//(use relative path for Unix systems)
    	File f = new File(path.toString());
    	//(works for both Windows and Linux)
    	f.getParentFile().mkdirs(); 
    	f.createNewFile();
    }
	
	private void deleteMirroFile(Path path){

//		System.out.println(path);
		try {
		    Files.delete(path);
		} catch (NoSuchFileException x) {
		    System.err.format("%s: no such" + " file or directory%n", path);
		} catch (DirectoryNotEmptyException x) {
		    System.err.format("%s not empty%n", path);
		} catch (IOException x) {
		    // File permission problems are caught here.
		    System.err.println(x);
		}
	}
}
