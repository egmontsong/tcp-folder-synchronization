package SyncServer;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;
import org.kohsuke.args4j.Argument;


public class ServerMain {
	public static int folder_size = 1;
	public static boolean recursive = false;
	@Option(name="-f",usage="Sets a folder")
	public static String dirpath = null;
	@Option(name="-p",usage="Sets a port")
	static int port = 4444;
	
    
	public static void main(String [] args) throws IOException, InterruptedException{
		new ServerMain().parsecmd(args);
		Path path = Paths.get(dirpath).toAbsolutePath();

//		System.out.println(path);
		
		SyncDir syncdir = new SyncDir(path);
		syncdir.createSycnfileForAll();
		
		ServerInstService ServerService = new ServerInstService(syncdir.SyncFileList, path, port);
		ServerService.createTCPserver();	
		
	}	
	
	public void parsecmd(String[] args) throws IOException {
        CmdLineParser parser = new CmdLineParser(this);
        
        // if you have a wider console, you could increase the value;
        // here 80 is also the default
        parser.setUsageWidth(80);

        try {
            // parse the arguments.
            parser.parseArgument(args);

        } catch( CmdLineException e ) {
            // if there's a problem in the command line,
            // you'll get this exception. this will report
            // an error message.
            System.err.println(e.getMessage());
            System.err.println("java SampleMain [options...] arguments...");
            // print the list of available options
            parser.printUsage(System.err);
            System.err.println();

            return;
        }


    }
}